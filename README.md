<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

## Laravel Backpack

### Thing todo list
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-backpack.git`
2. Go to folder: `cd laravel-backpack`
3. Run `composer install`
4. Run `cp .env.example .env` then write your credential db.
5. Run `php artisan migrate`
6. Open your favorite browser: http://localhost:8000/admin/login

### Source
Official site: https://backpackforlaravel.com/

Demo: https://demo.backpackforlaravel.com/admin/login

### Screen shot

Register New User

![Register New User](img/register.png "Register New User")

User Login

![User Login](img/login.png "User Login")

Welcome Page

![Welcome Page](img/welcome.png "Welcome Page")

User Profile Page

![User Profile Page](img/profile.png "User Profile Page")

Add New Tag

![Add New Tag](img/add_tags.png "Add New Tag")

List All Tags
![List All Tags](img/tags.png)  
